def app(environ, start_response):
    data = 'Hello, World!\n'
    start_response("200 OK", [
        ("Content-Type", "text/plain"),
        ("Content-Length", str(len(data)))
    ])
    id = iter([data])
    print id
    return id
